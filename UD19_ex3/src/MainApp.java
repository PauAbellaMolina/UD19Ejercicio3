import java.awt.EventQueue;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainApp extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainApp frame = new MainApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private JPanel contentPane;
	private JRadioButton rdbtnNewRadioButton;
	private JRadioButton rdbtnNewRadioButton_1;
	private JRadioButton rdbtnNewRadioButton_2;
	private JCheckBox chckbxNewCheckBox;
	private JCheckBox chckbxNewCheckBox_1;
	private JCheckBox chckbxNewCheckBox_2;
	private JSlider slider;
	
	public MainApp() {
		

		//Aqu� a�ado su t�tulo
		setTitle("Responde");
		//Aqu� indico sus dimensiones
		setBounds(100, 100, 247, 571);
		//Aqu� hago que cuando se cierre la ventana, la aplicaci�n se cierre
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		//Hago visible la ventana
		setVisible(true);	
		//Creamos el contentPane y indicamos su dise�o
		contentPane = new JPanel();
		//Aqu� asigno el panel a la ventana
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		//Creaci�n de componentes
		
		//Aqu� creo la selecci�n tipo radio de la encuesta, solo se puede seleccionar una a la vez
		JLabel lblNewLabel = new JLabel("Elije un sistema operativo");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel, 10, SpringLayout.WEST, contentPane);
		contentPane.add(lblNewLabel);
		
		rdbtnNewRadioButton = new JRadioButton("Windows");
		rdbtnNewRadioButton.setSelected(true);
		sl_contentPane.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton, 6, SpringLayout.SOUTH, lblNewLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(rdbtnNewRadioButton);

		
		rdbtnNewRadioButton_1 = new JRadioButton("Linux");
		rdbtnNewRadioButton_1.setSelected(true);
		sl_contentPane.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_1, 6, SpringLayout.SOUTH, rdbtnNewRadioButton);
		sl_contentPane.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton_1, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(rdbtnNewRadioButton_1);
		
		rdbtnNewRadioButton_2 = new JRadioButton("Mac");
		rdbtnNewRadioButton_2.setSelected(true);
		sl_contentPane.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_2, 6, SpringLayout.SOUTH, rdbtnNewRadioButton_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton_2, 10, SpringLayout.WEST, contentPane);
		contentPane.add(rdbtnNewRadioButton_2);
		
		//Aqu� agrupo todos los radios en un solo grupo
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rdbtnNewRadioButton);
		bgroup.add(rdbtnNewRadioButton_1);
		bgroup.add(rdbtnNewRadioButton_2);
		
		//Aqu� creo una selecci�n tipo checkbox, donde se puede seleccionar o ninguno o muchos
		JLabel lblNewLabel_1 = new JLabel("Elije tu especialidad");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 21, SpringLayout.SOUTH, rdbtnNewRadioButton_2);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel_1, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(lblNewLabel_1);
		
		chckbxNewCheckBox = new JCheckBox("Programaci\u00F3n");
		sl_contentPane.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox, 6, SpringLayout.SOUTH, lblNewLabel_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, chckbxNewCheckBox, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(chckbxNewCheckBox);
		
		chckbxNewCheckBox_1 = new JCheckBox("Dise\u00F1o gr\u00E1fico");
		sl_contentPane.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_1, 6, SpringLayout.SOUTH, chckbxNewCheckBox);
		sl_contentPane.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_1, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(chckbxNewCheckBox_1);
		
		chckbxNewCheckBox_2 = new JCheckBox("Administraci\u00F3n");
		sl_contentPane.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_2, 6, SpringLayout.SOUTH, chckbxNewCheckBox_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_2, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(chckbxNewCheckBox_2);
		
		
		//Aqu� creo un slider para que el usuario indique sus horas de trabajo en el ordenador
		slider = new JSlider();
		slider.setValue(0);
		slider.setMaximum(10);
		sl_contentPane.putConstraint(SpringLayout.WEST, slider, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(slider);
		
		//Aqu� muestro el t�tulo indicando que ahora toca marcar las horas dedicadas
		JLabel lblNewLabel_2 = new JLabel("Horas dedicadas a tu ordenador");
		sl_contentPane.putConstraint(SpringLayout.NORTH, slider, 6, SpringLayout.SOUTH, lblNewLabel_2);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 22, SpringLayout.SOUTH, chckbxNewCheckBox_2);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel_2, 0, SpringLayout.WEST, lblNewLabel);
		contentPane.add(lblNewLabel_2);
		
		
		//Aqu� creo un  bot�n donde se enviar�n los datos en caso de que el usuario haga click
		JButton btnNewButton = new JButton("Enviar");
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNewButton, 37, SpringLayout.SOUTH, slider);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNewButton, 0, SpringLayout.EAST, lblNewLabel);
		btnNewButton.addActionListener(new ActionListener() {
			//Aqu� ejecuto el m�todo donde mostrar� todo lo que ha seleccionado el usuario
			public void actionPerformed(ActionEvent e) {
				mostrarSeleccionado();
			}
		});	
		contentPane.add(btnNewButton);	
	}
		public void mostrarSeleccionado() {
			
			//Aqu� creo una serie de condiciones para acabar mostrando el sistema operativo que ha seleccionado el usuario
			System.out.println("El sistema operativo que has seleccionado �s: ");

			if(rdbtnNewRadioButton.isSelected()) {
				System.out.println(rdbtnNewRadioButton.getText());
			}
			if(rdbtnNewRadioButton_1.isSelected()) {
				System.out.println(rdbtnNewRadioButton_1.getText());
			}
			if(rdbtnNewRadioButton_2.isSelected()) {
				System.out.println(rdbtnNewRadioButton_2.getText());
			}
			else {
				System.out.println("ERROR: Escoge una de las 3 opciones anteriores");
			}
			
			//Aqu� creo una serie de condiciones para acabar mostrando las especialidades que ha elegido el usuario
			System.out.println("Tu especialidad �s: ");
			
			if(chckbxNewCheckBox.isSelected()) {
				System.out.println(chckbxNewCheckBox.getText());
			}
			if(chckbxNewCheckBox_1.isSelected()) {
				System.out.println(chckbxNewCheckBox_1.getText());
			}
			if(chckbxNewCheckBox_2.isSelected()) {
				System.out.println(chckbxNewCheckBox_2.getText());
			}
			
			//Aqu� muestro las horas de trabajo en el ordenador del usuario, a trav�s del slider
			System.out.println("Horas dedicadas al ordenador: ");
			System.out.println(slider.getValue() + "Horas");
		}
	};

